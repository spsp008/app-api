const APP_CONSTANTS = {
  USER: {
    ERRORS: Object.freeze({
      INVALID: 'Invalid user.',
      CREATE: 'Unable to create user.',
      GET: 'Unable to get user(s)'
    })
  },
  ERRORS: Object.freeze({
    INVALID_DATE: 'Invalid date.',
    NOT_FOUND: 'Not found.',
    SOMETHING_WENT_WRONG: 'Something went wrong.'
  }),
  STATUS: Object.freeze({
    SUCCESS: 'SUCCESS',
    FAILURE: 'FAILURE'
  })
};

module.exports = Object.freeze(APP_CONSTANTS);
