'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Items', [
      {
        name: "T shirt"
      },
      {
        name: "Jeans"
      },
      {
        name: "Computer"
      },
      {
        name: "Wall fan"
      },
      {
        name: "Induction"
      },
      {
        name: "Eye glasses"
      },
      {
        name: "Biscuits"
      },
      {
        name: "Mobile"
      },
      {
        name: "TV"
      },
      {
        name: "Car"
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Items', null, {});
  }
};
