const express = require('express');
const router = express.Router();
const { verifySignUp, authJwt } = require("../middleware");

const {
  create,
  login,
  get,
  getAll,
  logout
} = require('../controllers/users.controller.js');

router.get('/list', getAll);
router.get('/:id', get);
router.post('/create', [verifySignUp.checkDuplicateUsername], create);
router.post('/login', login);
router.post('/logout', [authJwt.verifyToken], logout);

module.exports = router;
