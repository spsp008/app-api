const express = require('express');
const router = express.Router();
const dotenv = require('dotenv').config();

const userRoutes = require('./user.routes.js');
const itemRoutes = require('./item.routes.js');
const cartRoutes = require('./cart.routes.js');
const orderRoutes = require('./order.routes.js');

router.get('/', (req, res) => {
  res.send('Hello World!' + process.env.NODE_ENV)
});

router.use('/user', userRoutes);
router.use('/item', itemRoutes);
router.use('/cart', cartRoutes);
router.use('/order', orderRoutes);

module.exports = router;
