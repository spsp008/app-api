const express = require('express');
const router = express.Router();
const { authJwt } = require("../middleware");

const {
  getAll
} = require('../controllers/orders.controller');

router.get('/list', [authJwt.verifyToken], getAll);

module.exports = router;
