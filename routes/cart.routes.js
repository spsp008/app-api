const express = require('express');
const router = express.Router();
const { authJwt } = require("../middleware");

const {
  addItem,
  getAll,
  orderItems,
  getCartItems
} = require('../controllers/carts.controller');

router.post('/add', [authJwt.verifyToken], addItem);
router.get('/list', [authJwt.verifyToken], getAll);
router.get('/items/list', [authJwt.verifyToken], getCartItems);
router.post('/:cart_id/complete', [authJwt.verifyToken], orderItems);

module.exports = router;
