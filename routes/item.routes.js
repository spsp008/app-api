const express = require('express');
const router = express.Router();
const { authJwt } = require("../middleware");

const {
  create,
  getAll
} = require('../controllers/items.controller');

router.post('/create', create);
router.get('/list', [authJwt.verifyToken], getAll);

module.exports = router;
