const Models = require('../models');
const Order = Models.Order;
const CartItem = Models.CartItem;
const Item = Models.Item;
const { sequelize } = Models;
const APP_CONSTANTS = require('../constants.js');

const getAll = (req, res) => {
  const { currentUser } = req;
  return Order.findAll({where: {cart_id: currentUser.cart_id, user_id: currentUser.id}, include: CartItem
 })
  .then((orders) => res.status(200).send({status: true, orders}))
  .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

module.exports = {
  getAll,
};
