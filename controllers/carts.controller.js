const Models = require('../models');
const Cart = Models.Cart;
const CartItem = Models.CartItem;
const Item = Models.Item;
const Order = Models.Order;
const { sequelize } = Models;
const APP_CONSTANTS = require('../constants.js');

const getAll = (req, res) => {
  return Cart.findAll()
    .then((carts) => res.status(200).send(carts))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

const addItem = async (req, res) => {
  const { item_id } = req.body;
  if (!item_id) res.status(422).send({message: 'item is required', status: false});

  const item = await Item.findOne({where: { id: item_id}});
  if (!item) res.status(422).send({message: 'invalid item', status: false});
  const { currentUser } = req;
  const { cart_id } = currentUser;
  // first load unpurchased cart item
  const cartItem = await CartItem.findOne({where: { cart_id, item_id, is_purchased: false}});
  if (cartItem) {
    // if there is already an unpurchased cart item increase quantity by 1
    const quantity = (cartItem.quantity ? cartItem.quantity : 0) + 1;
    return cartItem.update({
      quantity: quantity + 1
    })
      .then((result) => res.status(201).send({message: 'Item added', status: true}))
      .catch((error) => res.status(500).send(error))
  } else {
    // else create new
    return CartItem.create({
      cart_id,
      item_id,
      quantity: 1
    })
      .then((result) => res.status(201).send({message: 'Item added', status: true}))
      .catch((error) => res.status(500).send(error));
  }
};

const orderItems = async (req, res) => {
  const {cart_id} = req.params;
  const { currentUser } = req;
  // check cart_id user's or not
  if (+cart_id !== +currentUser.cart_id) {
    return res.status(401).send({message: 'Not authorised to add items to this cart', status: false});
  }
  // create order
  await Order.create({cart_id, user_id: currentUser.id})
    .then((order) => {
        // update cart items as purchased and set its order_id
      CartItem.update({is_purchased: true, order_id: order.id}, { where: {cart_id, is_purchased: false}});
      return res.status(201).send({status: true, order});
    })
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

const getCartItems = (req, res) => {
  const {currentUser} = req;
  const {cart_id} = currentUser;
  return CartItem.findAll({where: {cart_id, is_purchased: false}})
    .then((carts) => res.status(200).send(carts))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

module.exports = {
  addItem,
  getAll,
  getCartItems,
  orderItems
};
