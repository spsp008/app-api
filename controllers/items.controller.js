const Models = require('../models');
const Item = Models.Item;
const { sequelize } = Models;
const APP_CONSTANTS = require('../constants.js');

const create = (req, res) => {
  const { name } = req.body;
  if (!name) res.status(422).send({message: 'name is required', status: false})
  return Item.create({
    name
  })
    .then((item) => res.status(201).send(item))
    .catch((error) => res.status(500).send(error));
};

const getAll = (req, res) => {
  return Item.findAll()
    .then((items) => res.status(200).send(items))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

module.exports = {
  create,
  getAll
};
