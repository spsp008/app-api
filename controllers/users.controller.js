const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const Models = require('../models');
const User = Models.User;
const Cart = Models.Cart;
const config = require("../config/auth.config.js");
const { sequelize } = Models;
const APP_CONSTANTS = require('../constants.js');

const create = (req, res) => {
  const { user_name, password, name } = req.body;
  return User.create({
    user_name,
    password: bcrypt.hashSync(password, 8),
    name
  })
    .then((user) => {
      return Cart.create({user_id: user.id}).then(cart => {
        user.update(
          { cart_id: cart.id }
        ).then((result) =>  {
          res.status(201).send({message: "user created", status: true});
        });
      });
    })
    .catch((error) => res.status(500).send(error));
};

const get = (req, res) => {
  const { id } = req.params;
  return User.findOne({
    id,
  })
    .then((user) => res.status(200).send(user))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

const getAll = (req, res) => {
  return User.findAll()
    .then((users) => res.status(200).send(users))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

const login = (req, res) => {
  const {user_name, password } = req.body;
  User.findOne({
    where: {
      user_name
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      const passwordIsValid = bcrypt.compareSync(
        password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          token: null,
          message: "Invalid Password!"
        });
      }

      if (user.token) {
        jwt.verify(user.token, config.secret, (err, decoded) => {
          if (err) {
            const {id, user_name, name, cart_id} = user;
            const token = jwt.sign({ id, user_name, name, cart_id }, config.secret, {
              expiresIn: 86400 // 24 hours
            });
            user.update({token});
            return res.status(200).send({status: true, user: {
              id: user.id,
              user_name,
              cart_id,
              token
            }});
          }
          return res.status(405).send({
            status: false,
            message: "You are already logged in from another device! Logout from there"
          });
        });
      }
      const {id, user_name, name, cart_id} = user;
      const token = jwt.sign({ id, user_name, name, cart_id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });
      user.update({token});
      return res.status(200).send({status: true, user: {
        id: user.id,
        user_name,
        cart_id,
        token
      }});
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};

const logout = (req, res) => {
  const {currentUser} = req;
  const {id} = currentUser;
  return User.update({token: null}, { where: {id}})
    .then((result) => res.status(200).send({status: true, message: 'Logged out successfully'}))
    .catch((error) => res.status(500).send(APP_CONSTANTS.ERRORS.SOMETHING_WENT_WRONG));
};

module.exports = {
  create,
  login,
  get,
  getAll,
  logout
};
