const Models = require("../../models");
const User = Models.User;

checkDuplicateUsername = (req, res, next) => {
  const { user_name, password, name } = req.body;
  if (!user_name || !password || !name) {
    return res.status(422).send({status: false, message: 'user_name or password or name is missing'});
  }
  // Username
  User.findOne({
    where: {
      user_name
    }
  }).then(user => {
    if (user) {
      res.status(400).send({
        message: "Failed! Username is already in use!"
      });
      return;
    }

    next();
  });
};

const verifySignUp = {
  checkDuplicateUsername,
};

module.exports = verifySignUp;
