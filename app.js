const express = require('express');
const app = express();
const port = 4000;
const cors = require('cors')
const dotenv = require('dotenv').config();
const routes = require('./routes');

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cors());

app.use(routes);

app.listen(port, () => {
  console.log(
    `Sezzle app listening at ${port} in ${process.env.NODE_ENV} environment!`
  );
});
