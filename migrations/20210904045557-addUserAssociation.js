'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Users', // name of Target model
      'cart_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Carts', // name of Source model
          key: 'id',
        },
        onDelete: 'SET NULL',
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Users', // name of Source model
      'cart_id' // key we want to remove
    );
  }
};
