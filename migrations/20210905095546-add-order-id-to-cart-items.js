'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'CartItems', // name of Target model
      'order_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Orders', // name of Source model
          key: 'id',
        },
        onDelete: 'SET NULL',
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'CartItems', // name of Source model
      'order_id' // key we want to remove
    );
  }
};
