'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Carts',
      'is_purchased'
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Carts',
      'is_purchased',
      {
        type: Sequelize.BOOLEAN,
      }
    );
  }
};
