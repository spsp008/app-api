
# Shopping Cart-API


## Environment
- NodeJs [v13+](https://nodejs.org/en/download/)
## Packages used
- Express [v4.17.1](https://www.npmjs.com/package/express)
- Sequelize [v6.6.4](https://www.npmjs.com/package/sequelize)
- PostgreSQL [v8.6.0](https://www.npmjs.com/package/pg)

## Cloud service used
- Amazon EC2 for API
- Amazon RDS for Postgres DB
- Amazon S3 for UI files static hosting

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install dependancies. In the root folder run below command.

```bash
npm install
```

## Usage
To start app
- First create `.env` file in the root folder and set environment either of *local|dev|test|env* e.g.
```
NODE_ENV=local
```
In the config folder create *environment.config.json* file e.g. `local.config.json` and add following values according to your database. First create a DB in postgres with name say `saas`. Dev and Test config files are already added. Example values below
```
  {
    "username": "shiv",
    "password": "password",
    "database": "saas",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "port": 5432
  }
```
Then run migration and seeds respectively.
Note: Sequelize cli must be installed before running this.
```
sequelize db:create // In case db is not already created
```
```
sequelize db:migrate
```
```
sequelize db:seed:all
```
This will create **User**, **Cart**, **Item**, **CartItem** and **Order** table in DB and associations among them. **Item** table must be filled with the data.
To run server run

```bash
npm start
```
