'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Item.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'Item',
  });

  Item.associate = function(models) {
    // associations can be defined here
    Item.belongsToMany(models.Cart, {
      through: 'CartItems',
      as: 'carts',
      foreignKey: 'item_id',
      otherKey: 'cart_id'
    });
  };
  return Item;
};
